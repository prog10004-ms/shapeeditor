"""
This module defines the Circle class.

@Author: Prof. Magdin Stoica
@Course: Programming Priciples, PROG10004
"""
import math
from shape import Shape
import turtle

class Circle(Shape):
    """
    Represents circle objects and associated functionality such as calculating areas, expanding
    contracting and drawing.

    Attributes:
        - radius : float
    """

    def __init__(self, radius = 10):
        """The constructor method defines the field variables that make-up circle objects"""
        #ensure the base portion of the rectangle is initialized
        Shape.__init__(self)
        
        self._radius = radius

    def calculateArea(self):
        """Calculates and returns the area of the circle object using the given formula"""
        area = math.pi * (self._radius ** 2)
        return area

    def getRadius(self):
        """Accessor method that provides access to the value of the _radius field variable"""
        return self._radius

    def setRadius(self, newRadius):
        """Mutator method that allows the modification of the value of the _radius field variable"""
        self._radius = newRadius

    #TODO: define a method that will expand the circle by increasing its radius by a given amount

    #TODO: define a method that will contract the circle by decreasing its radius by a given amount
 
    def draw(self, penSize):
        #create a pen as a Turtle object and set its properties: color, pen size
        pen = turtle.Turtle()
        pen.color(self._color)
        pen.pensize(penSize)

        #position the pen in the centre of the shape 
        #TODO how can you calculate the position of one of the vertices of the shape
        pen.setpos(self._location[0], self._location[1])

        #calculate the number of sides and the angle of a circle-polygon aproximation
        SIDE_LENGTH = 2
        numSides = int((2 * math.pi * self._radius) / SIDE_LENGTH)
        angle = (180 * (numSides - 2)) / numSides

        #repeat drawing a polygon side _numSides times
        for _ in range(numSides):
            #using the pen draw forward _sideLength
            pen.forward(SIDE_LENGTH)

            #turn the pen using the shape exterior angle
            pen.left(180 - angle)
