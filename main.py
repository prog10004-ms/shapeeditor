"""
Main module for a program that has the following requirements:

Software Requirements
Create an object-oriented program that allows the user to define shapes such as 
rectangle and circles, displays them and calculates their area using the following 
formulas:
    - Circle Area =  PI * R2, where PI = 3.14
    - Rectangle Area = Length * Width

Object-Oriented Analysis
Nouns: program, shape(s), rectangle(s), circle(s), area, formula, PI, 3.14, radius, 
length, width

Object-Oriented Design
Classes: Program, Circle, Rectangle, float, math.pi 

@Author: Prof. Magdin Stoica
@Course: Programming Priciples, PROG10004
"""

from program import Program

#create the program object
prog = Program()

#ask the program object to run
prog.run()