"""
Main module for a shape editor program that has the following requirements:

Software Requirements
Create an object-oriented program that allows the user to define shapes and draw such as 
rectangle and circles and polygons. For each shape the specifies the location of the shape,
the colour and whether it is filled or not. Before drawing the user can specify the width
of the pen being used to draw the shape.

Object-Oriented Analysis
Nouns: program, shape(s), rectangle(s), circle(s), area, formula, PI, 3.14, radius, 
length, width

Object-Oriented Design
Classes: Program, Circle, Rectangle, float, math.pi 

@Author: Prof. Magdin Stoica
@Course: Programming Priciples, PROG10004
"""

from shapeeditor import ShapeEditor

#create the shape editor object
editor = ShapeEditor()

#ask the editor object to run
editor.run()