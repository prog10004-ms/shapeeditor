"""
This module defines the Polygon class.

@Author: Prof. Magdin Stoica
@Course: Programming Priciples, PROG10004
"""
from shape import Shape
import turtle

class Polygon(Shape):
    """
    Represents polygon objects and associated functionality such as calculating areas, expanding
    contracting and drawing.

    Attributes:
        - numSides : int
        - sideLength: float
    """

    def __init__(self, numSides, sideLength):
        """The constructor method defines the field variables that make-up circle objects"""
        #ensure the base portion of the rectangle is initialized
        Shape.__init__(self)
        
        self._numSides = numSides
        self._sideLength = sideLength

    def getNumOfSides(self):
        """Accessor method that provides access to the value of the _numSides field variable"""
        return self._numSides

    def setNumOfSides(self, newNumSides):
        """Mutator method that allows the modification of the value of the _numSides field variable"""
        self._numSides = newNumSides

    def getSideLength(self):
        """Accessor method that provides access to the value of the _sideLength field variable"""
        return self._sideLength

    def setSideLength(self, newSideLength):
        """Mutator method that allows the modification of the value of the _sideLength field variable"""
        self._sideLength = newSideLength

    def getAngle(self):
        """Calculates the interior angle of regular polygon with the given the number of sides. This is 
        an example of a derived (calculated) attribute"""
        return (180 * (self._numSides - 2)) / self._numSides

    def calculateArea(self):
        """Calculates and returns the area of the polygon object"""
        #TODO: determine and use the formula to be used to calculate the area of a regular polygon
        assert False, "Not implemented"

    def draw(self, penSize):
        #create a pen as a Turtle object and set its properties: color, pen size
        pen = turtle.Turtle()
        pen.color(self._color)
        pen.pensize(penSize)

        #position the pen in the centre of the shape 
        #TODO how can you calculate the position of one of the vertices of the shape
        pen.setpos(self._location[0], self._location[1])

        #calculate the angle outside of the loop for performance reasons
        angle = self.getAngle()

        #repeat drawing a polygon side _numSides times
        for _ in range(self._numSides):
            #using the pen draw forward _sideLength
            pen.forward(self._sideLength)

            #turn the pen using the shape exterior angle
            pen.left(180 - angle)
