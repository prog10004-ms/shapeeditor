"""
Represents the program the user is running and is responsible for the user interactivity
concern.

@Author: Prof. Magdin Stoica
@Course: Programming Priciples, PROG10004
"""
from circle import Circle
from rectangle import Rectangle

class Program:
    def run(self):
        """The 'main' method of the program that coordinates the execution and the interation of all its objects"""
        #create a circle with a default radius
        defaultCircle = Circle()

        #calculate the area of the circle
        area = defaultCircle.calculateArea()

        #print the area of the circle with its default radius
        print(f'The area of a circle with the radius {defaultCircle.getRadius()} is {area}')

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        
        #create a circle with the radius 25
        smallCircle = Circle(25)

        #calculate the area
        area = smallCircle.calculateArea()

        #print the area of the circle
        print(f'The area of a circle with the radius {smallCircle.getRadius()} is {area}')

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

         #ask the user for a radius 
        userRadius = float(input('Please enter a radius: '))

        #create a circle with the given radius (userCircle)
        userCircle = Circle(userRadius)

        #caculate the area
        area = userCircle.calculateArea()

        #print the area of the user circle
        print(f'The area of a circle with the radius {userCircle.getRadius()} is {area}')

        #TODO: create a rectangle using user provided dimensions

        #TODO: calculate the area of the rectangle

        #TODO: print the rectangle dimensions and its area

        #TODO: expand and contract circles and rectangles using new methods defined for that purpose 