"""
This module defines the Rectangle class.

@Author: Prof. Magdin Stoica
@Course: Programming Priciples, PROG10004
"""
from shape import Shape
import turtle

class Rectangle(Shape):
    """
    Represents rectangle objects and associated functionality such as calculating areas, expanding
    contracting and drawing.

    Attributes:
        - _length : float
        - _width : float
    """

    def __init__(self, width = 20, length = 10):
        """The constructor method defines the field variables that make-up rectangle objects"""
        #ensure the base portion of the rectangle is initialized
        Shape.__init__(self)

        self._width = width
        self._length = length

    def calculateArea(self):
        """Calculates and returns the area of the rectangle object using the given formula"""
        area = self._width * self._length
        return area

    def getLength(self):
        """Accessor method that provides access to the value of the _length field variable"""
        return self._length

    def setLength(self, newLength):
        """Mutator method that allows the modification of the value of the _length field variable"""
        self._length = newLength

    def getWidth(self):
        """Accessor method that provides access to the value of the _width field variable"""
        return self._width

    def setWidth(self, newWidth):
        """Mutator method that allows the modification of the value of the _width field variable"""
        self._width = newWidth

    #TODO: define a method that will expand the rectangle by increasing its length and width by a given amount

    #TODO: define a method that will contract the rectangle by decreasing its length and width  by a given amount

    def draw(self, penSize):
        #create a "pen" in the form of a turtle object
        pen = turtle.Turtle()
        pen.color(self._color)
        pen.pensize(penSize)

        #calculate the position of the bottom-left corner of the rectangle
        xPos = self._location[0] - self._width / 2
        yPos = self._location[1] - self._length / 2
        
        #position the pen at location of the bottom-left corner
        pen.up()
        pen.setpos(xPos, yPos)
        pen.down()

        #repeat twice drawing the length and the width of the rectangle
        for _ in range(2):
            #draw the width of the rectangle
            pen.forward(self._width)

            #rotate to prepare for length draw
            pen.left(90)

            #draw the length of the rectangle
            pen.forward(self._length)

            #rotate for the next segment
            pen.left(90)
