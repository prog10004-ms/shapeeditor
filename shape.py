"""Generalization of any shape represented by a Shape base class"""

class Shape:
    def __init__(self):
        self._color = 'black'
        self._location = (0, 0)
        self._isFilled = False

    def getColor(self):
        return self._color

    def setColor(self, newColor):
        self._color = newColor

    def getLocation(self):
        return self._location

    def setLocation(self, newLocation):
        self._location = newLocation

    def getIsFilled(self):
        return self._isFilled

    def setIsFilled(self, isFilled):
        self._isFilled = isFilled

    def draw(self, penSize):
        #defensive programming: this should NEVER be called. This is a base class
        assert False, 'Unexpected request to draw a generic shape. Cannot draw shape. Use a concrete shape instance instead.'
