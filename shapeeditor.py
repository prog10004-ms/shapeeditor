"""
Represents the shape editor the user is running and is responsible for the user interactivity
concern.

@Author: Prof. Magdin Stoica
@Course: Programming Priciples, PROG10004
"""
import turtle
from rectangle import Rectangle
from circle import Circle
from polygon import Polygon

class ShapeEditor:
    """
    Represents shape editor and associated functionality such as displaying the GUI window
    and interacting with the user to decide what shapes to drawm, where and how

    Attributes:
        - _screen : turtle.Screen
    """ 
    def __init__(self):
        turtle.speed(0)
        self._screen = turtle.Screen()
        self._screen.bgcolor('pink')  
        self._screen.delay(0) 

    def run(self):
        #ask the user for the type of shape to create: circle, rectangle, polygon
        shapeType = self.askUserForShapeType()

        #ask the user where to location the shape
        xPos, yPos = self.askUserForShapeLocation()

        #create the appropriate shape
        shape = self.createShape(shapeType, xPos, yPos)
        
        #ask the user for the pen size
        penSize = self.askUserForPenSize()

        #ask the shape to draw itself
        shape.draw(penSize)

        #prevent the screen editor from closing
        self._screen.exitonclick()

    def askUserForShapeType(self):
        shapeType = self._screen.textinput('Shape Editor', 'Please enter a shape type [circle, rectangle, polygon]')
        #TODO: ensure the correct input is provided by the user
        return shapeType

    def askUserForShapeLocation(self):
        #ask for the x coordinate
        xPos = self._screen.numinput('Shape Editor', 'Please enter the x-coordinate for your shape:', 0, -400, 400)

        #ask for the y coordinate
        yPos = self._screen.numinput('Shape Editor', 'Please enter the y-coordinate for your shape:', 0, -400, 400)
        return (xPos, yPos)
    
    def askUserForPenSize(self):
        penSize = self._screen.numinput('ShapeEditor', 'Please enter the width of the pen used to draw shapes:', 1, 1, 10)
        return penSize

    def askForShapeColour(self):
        shapeColor = self._screen.textinput('Shape Editor', 'Please enter the shape color:')   
        return shapeColor

    def createShape(self, shapeType, xPos, yPos):
        #depending on shapType create the appropriate shape, set its origin and return it
        shape = None
        if shapeType == 'rectangle':
            shape = self.createRectangle(xPos, yPos)
        elif shapeType == 'circle':
            shape = self.createCircle(xPos, yPos)
        elif shapeType == 'polygon':
            shape = self.createPolygon(xPos, yPos)
        else:
            #defensive programming: ensure the program asserts what the valid shape types are
            assert False, 'Unexpected shape type. Cannot create shape.'

        #ask and set the shape colour
        shapeColor = self.askForShapeColour()
        shape.setColor(shapeColor)
        return shape

    def createRectangle(self, xPos, yPos):
        #ask for shape dimensions
        width = self._screen.numinput('Shape Editor', 'Please enter the width of the rectangle:', 200, 1, 600)
        length = self._screen.numinput('Shape Editor', 'Please enter the length of the rectangle:', 200, 1, 600)
  
        rect = Rectangle(width, length)
        rect.setLocation((xPos, yPos))

        return rect

    def createCircle(self, xPos, yPos):
        radius = self._screen.numinput('Shape Editor', 'Please enter the radius of the circle:', 200, 1, 600)
        circle = Circle(radius)  
        circle.setLocation((xPos, yPos))

        return circle     

    def createPolygon(self, xPos, yPos):
        #ask for shape dimensions
        numSides = int(self._screen.numinput('Shape Editor', 'Please enter the number of sides of the regular polygon:', 3, 3, 100))
        sideLength = self._screen.numinput('Shape Editor', 'Please enter the side length of the polygon:', 100, 1, 600)
  
        polygon = Polygon(numSides, sideLength)
        polygon.setLocation((xPos, yPos))

        return polygon